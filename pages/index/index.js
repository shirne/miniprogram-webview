// index.js
// 获取应用实例
const app = getApp()

Page({
  data: {
    url: app.globalData.rootUrl,
    lastUrl:app.globalData.rootUrl,
    isError: false,
  },
  onLoad() {
    if(app.globalData.currentUrl){
      this.setData({
        url: app.globalData.currentUrl
      })
    }
  },
  onShareAppMessage(options) {
    return {
      title: '您身边的植物医生',
      path: '/pages/index/index?url='+encodeURIComponent(options.webViewUrl),
      imageUrl: 'https://pic.dginfo.com/upfile/images/2020/08/03/09/6373204532159955836970879.jpg'
    }
  },
  onShareTimeline(){
    return {
      title: '您身边的植物医生',
      query: 'url=',
      imageUrl: 'https://pic.dginfo.com/upfile/images/2020/08/03/09/6373204532159955836970879.jpg'
    }
  },
  onWebLoad(e){
    console.log(e)
    if(e.detail.src != this.data.lastUrl)this.data.lastUrl = e.detail.src;
  },
  onWebMessage(msg){
    console.log(msg)
  },
  onWebError(e){
    console.log(e)
    if(this.data.isError){
      this.data.isError = false;
      return;
    }
    wx.showModal({content:'加载出错',complete:()=>{
      this.data.isError = true;
      this.setData({url:this.data.lastUrl})
    }})
  },
})
