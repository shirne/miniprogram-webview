// app.js
App({
  onLaunch(options) {
    if(options.query.url){
      this.globalData.currentUrl = options.query.url;
    }
  },
  onError(msg){
    console.log(msg)
  },
  globalData: {
    userInfo: null,
    rootUrl: 'https://app.dginfo.com/app/lvzan/'
  }
})
